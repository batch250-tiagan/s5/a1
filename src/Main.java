import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook()
                .setContact(new Contact("John Doe","+639152468596","Quezon City"))
                .setContact(new Contact("Jane Doe","+639162148573","Caloocan City"));
//                .setContact(new Contact().setName("No Contact").setAddress("Makati"))
//                .setContact(new Contact().setName("No Address").setContact("09123456789"));

//        phonebook.printInfo("John Doe");
//        phonebook.printInfo("Jane Doe");
//        phonebook.printInfo("No Contact");
//        phonebook.printInfo("No Address");
//        phonebook.printInfo("No Name");
//
//        phonebook.setContact(new Contact());

        phonebook.printInfoAll();
    }
}