package com.zuitt.example;

public class Contact {

    private String name;
    private String contact;
    private String address;

    public Contact(){

    }

    public Contact(String name, String contact, String address){
            this.name = name;
            this.contact = contact;
            this.address = address;
    }

    public String getName() {
        return name;
    }

    public Contact setName(String name) {
        this.name = name;
        return this;
    }

    public String getContact() {
        return contact;
    }

    public Contact setContact(String contact) {
        this.contact = contact;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Contact setAddress(String address) {
        this.address = address;
        return this;
    }

}
