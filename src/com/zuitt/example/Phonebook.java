package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;

    public Phonebook(){
        contacts = new ArrayList<>();
    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }


    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public Contact getContact(String name){
        for (Contact cont: contacts) {
            if(cont.getName() == name){
                return cont;
            }
        }
        return null;
    }

    public void printInfo(String name){
        Contact retrievedContact = getContact(name);
        if(retrievedContact != null){
            printName(retrievedContact);
            printContact(retrievedContact);
            printAddress(retrievedContact);
        }else{
            System.out.println("There is no contact with the given registered :" + name);
        }
    }

    public void printInfoAll(){
        for (Contact con: contacts) {
            printName(con);
            printContact(con);
            printAddress(con);
        }
    }

    private void printName(Contact retrievedContact){
        System.out.println("-------------------------------");
        System.out.println(retrievedContact.getName());
        System.out.println("-------------------------------");
    }

    private void printContact(Contact retrievedContact){
        System.out.println(retrievedContact.getName() +" has the following registered number:");
        if(retrievedContact.getContact() != null){
            System.out.println(retrievedContact.getContact());
        }else{
            System.out.println("There is no registered contacts");
        }
    }

    private void printAddress(Contact retrievedContact){
        System.out.println(retrievedContact.getName() +" has the following registered address:");
        if(retrievedContact.getAddress() != null){
            System.out.println(retrievedContact.getAddress());
        }else{
            System.out.println("There is no registered address");
        }
    }

    public Phonebook setContact(Contact contact) {
        if (contact.getName() != null){
            contacts.add(contact);
        }else{
            System.out.println("Contact has no registered Name");
        }
        return this;
    }

}
